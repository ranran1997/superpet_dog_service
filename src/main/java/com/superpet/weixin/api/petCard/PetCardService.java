package com.superpet.weixin.api.petCard;

import com.jfinal.plugin.activerecord.SqlPara;
import com.superpet.common.model.PetCard;

import java.util.List;

public class PetCardService {

    public static PetCardService me = new PetCardService();

    public List<PetCard> getPetCardList(Long userId){
        SqlPara sqlPara = PetCard.dao.getSqlPara("pets.getPetCardList",userId);
        return PetCard.dao.find(sqlPara);
    }

}
