package com.superpet.weixin.api.uploadFile;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.upload.UploadFile;
import com.superpet.common.MainConfig;
import com.superpet.common.kits.ImageKit;
import com.superpet.common.vo.WxSessionVo;
import org.joda.time.DateTime;

import java.io.File;

public class UploadFileService {

    public static final UploadFileService me = new UploadFileService();

    public static final  String PET_AVATAR = "petAvatar";
    public static final  String PET_SHOW = "petShow";
    public static final  String PET_CARD = "petCard";

    public static final int imageMaxSize = 1024 * 1024;
    public static final String uploadTempPath = "/upload/img/temp";
    private static final String basePath = "/upload/img/";
    public static final int FILES_PER_SUB_DIR = 5000;

    public String doUploadFile(WxSessionVo wxSessionVo, String uploadType, UploadFile uf) {
        String fileSize = uf.getFile().length() + "";
        String extName = "." + ImageKit.getExtName(uf.getFileName());

        String[] relativePathFileName = new String[1];
        String[] absolutePathFileName = new String[1];
        String[] fileName = new String[1];
        buildPathAndFileName(uploadType, wxSessionVo.getUserId(), extName, relativePathFileName, absolutePathFileName, fileName);
        saveOriginalFileToTargetFile(uf.getFile(), absolutePathFileName[0]);
        updateUploadCounter(uploadType);
        return relativePathFileName[0];
    }

    private String generateFileName(Long accountId, String extName) {
        DateTime dt = DateTime.now();
        return accountId + "_" + dt.toString("yyyyMMddHHmmss") + extName;
    }

    private void updateUploadCounter(String uploadType) {
        Db.update("update p_upload_counter set counter = counter + 1 where uploadType=? limit 1", uploadType);
    }

    private void buildPathAndFileName(
            String uploadType,
            Long accountId,
            String extName,
            String[] relativePathFileName,
            String[] absolutePathFileName,
            String[] fileName) {

        Integer counter = Db.queryInt("select counter from p_upload_counter where uploadType=? limit 1", uploadType);
        if (counter == null) {
            throw new IllegalArgumentException("uploadType 不正确");
        }

        String relativePath = "/" + (counter / FILES_PER_SUB_DIR) + "/";    // 生成相对对路径
        relativePath = basePath + uploadType + relativePath;

        fileName[0] = generateFileName(accountId, extName);
        relativePathFileName[0] =  relativePath + fileName[0];

        String absolutePath = MainConfig.uploadpath + relativePath;
        File temp = new File(absolutePath);
        if (!temp.exists()) {
            temp.mkdirs();
        }
        absolutePathFileName[0] = absolutePath + fileName[0];
    }

    private void saveOriginalFileToTargetFile(File originalFile, String targetFile) {
        originalFile.renameTo(new File(targetFile));
    }

    public String checkUploadFile(UploadFile uf) {
        if (uf == null || uf.getFile() == null) {
            return "上传文件为 null";
        }
        if (ImageKit.notImageExtName(uf.getFileName())) {
            uf.getFile().delete();
            return "只支持 jpg、jpeg、png、bmp 四种图片类型";
        }
        if (uf.getFile().length() > imageMaxSize) {
            uf.getFile().delete();
            return "图片尺寸只允许 "+imageMaxSize+"K 大小";
        }
        return null;
    }
}
