package com.superpet.common.intercept;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;
import com.superpet.common.kits.ConstantKit;
import com.superpet.weixin.api.login.LoginService;
import org.apache.commons.lang3.StringUtils;

public class ApiIntercept implements Interceptor {

    /**
     * API接口访问拦截器
     * 1、如果是以/api开头的action(登录除外)，则拦截，其他暂时先不拦截
     * 2、查看访问中是否有accessToken参数，且有值
     * 3、以accessToken为key值，查询缓存及数据库中是否有合法的openid和sessionKey
     * 4、查询成功，则进入controller，否则判定为非法请求
     * @param inv
     */
    public void intercept(Invocation inv) {
        String actionKey = inv.getActionKey();
        System.out.println("actionKey===="+actionKey);
        if("/api/wx/login/doLogin".equalsIgnoreCase(actionKey)) {
            inv.invoke();
        }else if(actionKey.startsWith("/api")){
            if(inv.getController().getRequest().getHeader("Content-Type").contains("multipart/form-data")){
                inv.getController().getFile();
            }
            String accessToken = inv.getController().getPara("accessToken");
            System.out.println("accessToken===="+accessToken);
            if(StringUtils.isNotBlank(accessToken) && LoginService.me.hasLogin(accessToken)){
                inv.invoke();
            }else{
                inv.getController().renderJson(Ret.fail("code", ConstantKit.CODE_FORBID).set("msg",ConstantKit.MSG_FORBID));
            }
        }else{
            inv.invoke();
        }
    }

}
