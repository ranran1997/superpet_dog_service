package com.superpet.common.kits;

import com.jfinal.kit.StrKit;

public class ImageKit {

	private final static String[] imgExts = new String[]{"jpg", "jpeg", "png", "bmp"};

	public static String getExtName(String fileName) {
		int index = fileName.lastIndexOf('.');
		if (index != -1 && (index + 1) < fileName.length()) {
			return fileName.substring(index + 1);
		} else {
			return null;
		}
	}

	public static boolean isImageExtName(String fileName) {
		if (StrKit.isBlank(fileName)) {
			return false;
		}
		fileName = fileName.trim().toLowerCase();
		String ext = getExtName(fileName);
		if (ext != null) {
			for (String s : imgExts) {
				if (s.equals(ext)) {
					return true;
				}
			}
		}
		return false;
	}

	public static final boolean notImageExtName(String fileName) {
		return ! isImageExtName(fileName);
	}

}


